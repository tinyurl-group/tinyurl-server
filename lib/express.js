const express = require('express');
const morgan = require('morgan');
const helmet = require('helmet');
const path = require('path');
const bodyParser = require('body-parser');
const glob = require('glob');
const { getClientIp } = require('request-ip');
const { parse } = require('useragent');
const { green, yellow, red, cyan, white } = require('chalk').default;
const logger = require('./logger');
const config = require('../config/config');

/**
 * Init express server
 */
module.exports.initServer = () => {
    const app = express();

    // init express middleware
    // Set query string parser
    app.use(bodyParser.urlencoded({
        extended: true
    }));

    // Set json form parser
    app.use(bodyParser.json());

    // Use helmet to secure Express headers
    app.use(helmet());

    // Add user ip and useragent to request
    app.use(
        /**
         * @param {any} req
         * @param {any} res
         */
        (req, res, next) => {
            req.clientIp = getClientIp(req);
            req.useragent = parse(req.headers['user-agent']);


            req.user = {};
            next();
        }
    );

    // Set express logger (morgan)
    app.use(morgan((tokens, req, res) => {
        /** @type {any} */
        const { user, useragent, clientIp } = req;
        const { statusCode } = res;
        let color = white;

        if (statusCode >= 500) {
            color = red;
        } else if (statusCode >= 400) {
            color = yellow;
        } else if (statusCode >= 300) {
            color = cyan;
        } else if (statusCode >= 200) {
            color = green;
        }

        const method = tokens.method(req, res);
        const url = tokens.url(req, res);
        const status = tokens.status(req, res);
        const contentLength = tokens.res(req, res, 'content-length');
        const responseTime = tokens['response-time'](req, res);

        const log = [
            green(method),
            url,
            color(status)
        ];

        if (config.env !== 'development') {
            if (user && user.username) {
                log.push(`- ${user.username}`);
            }

            log.push(`${clientIp}`);

            log.push(`- ${useragent.toString()}`);

            if (contentLength) {
                log.push(`- ${contentLength}`);
            }
        }

        log.push(`- ${responseTime} ms`);

        return log.join(' ');
    }, { stream: logger.stream }));

    // Load api routes
    const routes = glob.sync(path.resolve('./api/*/*/*.route.js'));
    routes.forEach((route) => {
        require(route)(app);
    });

    // Register server error routes
    initErrorRoutes(app);

    // Start the server
    return app;
};

/**
 * Register error routes
 */
function initErrorRoutes(app) {
    // Catch 404 and forward to error handler
    app.use((req, res, next) => {
        res.statusCode = 404;
        next(new Error(`The route: ${req.originalUrl} not found`));
    });

    // Catch all server error and response with 500 status code
    app.use((err, req, res, next) => {
        const response = {};

        if (res.statusCode === 404) {
            response.message = err.message || 'Not Found';
            response.statusCode = 404;
        } else {
            response.message = 'Server Error: An unexpected error occurred';
            response.statusCode = 500;

            if (config.env !== 'production') {
                response.stack = err.stack;
                response.message = err.message || 'Server Error: An unexpected error occurred';
            }

            console.error('Error in express error middleware', err, req.originalUrl);
        }

        // Response the error
        res.status(response.statusCode).json(response);
    });
}
