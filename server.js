// Load .env file to environment variables
require('dotenv').config();


const { green } = require('chalk').default;
const config = require('./config/config');
const mongoose = require('./lib/mongoose');
const logger = require('./lib/logger');
const { initServer } = require('./lib/express');


async function start() {
    try {
        // Connect to db
        await mongoose.connect();

        // Init express server
        const app = initServer();

        app.listen(config.port, config.host, () => {
            logger.info(green(`
${config.app.title}

Environment:     ${config.env}
URL:             ${config.url}
Database:        ${config.db.mongo.uri}
`));
        });
    } catch (error) {
        logger.error('Faild to start the server', error);
    }
}

start();
