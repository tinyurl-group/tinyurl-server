const express = require('express');
const shortid = require('shortid');
const { lookup } = require('dns');
const { promisify } = require('util');
const logger = require('../../../lib/logger');
const { Url } = require('./url.model');

const lookupAsync = promisify(lookup);

/**
 * Redirect to original url by tinyurl
 *
 * @param {express.Request} req
 * @param {express.Response} res
 */
async function redirectToUrl(req, res) {
    const { shortUrl } = req.params;

    try {
        const url = await Url.findOne({ shortUrl })
            .select('-_id originalUrl')
            .lean();

        if (!url) {
            return res.status(404).send(`Short url: "${shortUrl}" not found`);
        }

        res.redirect(url.originalUrl);
    } catch (error) {
        res.status(500).send(`An unexpected error occurred while try to get original url for ${shortUrl}`);
        logger.error(`Error while try to get original url for: "${shortUrl}" in redirectToUrl api`, error, req);
    }
}

/**
 * Get url
 *
 * @param {express.Request} req
 * @param {express.Response} res
 */
async function getUrl(req, res) {
    const { shortUrl, originalUrl } = req.query;

    const query = {};

    if (shortUrl) {
        query.shortUrl = shortUrl;
    } else if (originalUrl) {
        query.originalUrl = originalUrl;
    } else {
        return res.status(400).json({ message: 'Bad Request: shortUrl or originalUrl are missing from request query', statusCode: 400 });
    }

    try {
        const url = await Url.findOne(query)
            .select('-_id originalUrl shortUrl')
            .lean();

        if (!url) {
            return res.status(404).send('Url not found');
        }

        res.json(url);
    } catch (error) {
        res.status(500).json({ message: 'An unexpected error occurred while try to get url', statusCode: 500 });
        logger.error('Error while try to get url in getUrl api', error, req);
    }
}

/**
 * Create url
 *
 * @param {express.Request | any} req
 * @param {express.Response} res
 */
async function createUrl(req, res) {
    const { originalUrl } = req.body;

    if (!originalUrl) {
        return res.status(400).json({ message: 'Bad Request: originalUrl are missing from request body', statusCode: 400 });
    }

    if (!originalUrl.match(/^(http|https)/)) {
        return res.status(400).json({ message: 'Bad Request: originalUrl are not valid: missing protocol (http/https)', statusCode: 400 });
    }

    try {
        // Check if url already existed
        const existedUrl = await Url
            .findOne({ originalUrl })
            .select('-_id originalUrl shortUrl')
            .lean();

        if (existedUrl) {
            return res.json(existedUrl);
        }

        try {
            // Check if the url valid by dns lookup
            await lookupAsync(originalUrl.replace('https://', '').replace('http://', '').replace('/', ''));
        } catch (error) {
            return res.status(400).json({ message: 'Bad Request: originalUrl are not valid url', statusCode: 400 });
        }

        // Generate short non-sequential unique url
        const shortUrl = shortid.generate();

        // Add the new url
        const newUrl = new Url({
            originalUrl,
            shortUrl
        });

        // Save it
        await newUrl.save();

        res.status(201).json({ originalUrl, shortUrl });
    } catch (error) {
        res.status(500).json({ message: 'An unexpected error occurred while try to create new url', statusCode: 500 });
        logger.error('Error while try to create new url in createUrl api', error, req);
    }
}

module.exports = {
    getUrl,
    createUrl,
    redirectToUrl
};
