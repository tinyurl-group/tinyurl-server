const mongoose = require('mongoose');

const { Schema } = mongoose;

const urlSchema = new Schema({
    originalUrl: {
        type: String,
        trim: true,
        index: true,
        unique: 'originalUrl field is unique',
        required: 'originalUrl field is required'
    },
    shortUrl: {
        type: String,
        index: true,
        unique: 'shortUrl field is unique',
        required: 'shortUrl field is required'
    },
    createdDate: {
        type: Date,
        default: Date.now
    }
});

const Url = mongoose.model('Url', urlSchema);

module.exports = {
    Url
};
