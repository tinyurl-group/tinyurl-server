const express = require('express');
const { getUrl, createUrl, redirectToUrl } = require('./url.controller');

/**
 * Url routes
 *
 * @param { express.Express } app express application
 */
module.exports = (app) => {
    app.route('/:shortUrl')
        .get(redirectToUrl);


    app.route('/api/v1/url')
        .get(getUrl)
        .post(createUrl);
};
