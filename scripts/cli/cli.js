const { generateCrudApi } = require('./generator/crud.generator');

const command = process.argv[2];
const value = process.argv[3];

switch (command) {
    case 'generate':
    case 'g':
        generateCrudApi(value);
        break;

    default:
        break;
}
