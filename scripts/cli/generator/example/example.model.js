const mongoose = require('mongoose');

const { Schema } = mongoose;

const exampleSchema = new Schema({
    name: {
        type: String,
        trim: true,
        unique: 'name field is unique',
        required: 'name field is required'
    },
    createdBy: {
        type: String
    },
    createdDate: {
        type: Date,
        default: Date.now
    },
    updatedBy: {
        type: String
    },
    updatedDate: {
        type: Date
    }
});

const Example = mongoose.model('Example', exampleSchema);

module.exports = {
    Example
};
