const express = require('express');
const mongoose = require('mongoose');
const logger = require('../../lib/logger');
const { Example } = require('./example.model');

const { ObjectId } = mongoose.Types;
const selectedFields = '_id name createdDate';

/**
 * Get examples
 *
 * @param {express.Request} req
 * @param {express.Response} res
 */
async function getExamples(req, res) {
    const { name } = req.query;
    const query = {};

    if (name) {
        query.name = name;
    }

    try {
        const examples = await Example.find(query)
            .select(selectedFields)
            .lean();

        res.json(examples);
    } catch (error) {
        res.status(500).json({ message: 'An unexpected error occurred while try to get examples', statusCode: 500 });
        logger.error(`Error while try to get examples for query: "${JSON.stringify(query)}" in getExamples api`, error, req);
    }
}

/**
 * Get example by id
 *
 * @param {express.Request} req
 * @param {express.Response} res
 */
async function getExampleById(req, res) {
    const { exampleId } = req.params;

    try {
        const example = await Example.findById(exampleId)
            .select(selectedFields)
            .lean();

        if (!example) {
            return res.status(404).json({ message: `Example: "${exampleId}" not existed`, statusCode: 404 });
        }

        res.json(example);
    } catch (error) {
        res.status(500).json({ message: 'An unexpected error occurred while try to get example', statusCode: 500 });
        logger.error(`Error while try to get example for id: "${exampleId}" in getExample api`, error, req);
    }
}

/**
 * Create example
 *
 * @param {express.Request | any} req
 * @param {express.Response} res
 */
async function createExample(req, res) {
    const { user } = req;
    const { name } = req.body;

    if (!name) {
        return res.status(400).json({ message: 'Bad Request: Fields (name) are missing from request body', statusCode: 400 });
    }

    try {
        // Check if example already existed
        const existedExample = await Example.findOne({ name });

        if (existedExample) {
            return res.status(409).json({ message: `Conflict: The example name: "${name}" already existed`, statusCode: 409 });
        }

        // Add the new example
        const newExample = new Example({
            name,
            createdBy: user.id
        });

        // Save it
        const savedExample = await newExample.save();

        res.status(201).json(savedExample);
    } catch (error) {
        res.status(500).json({ message: 'An unexpected error occurred while try to create new example', statusCode: 500 });
        logger.error('Error while try to create new example in createExample api', error, req);
    }
}

/**
 * Update example
 *
 * @param {express.Request | any} req
 * @param {express.Response} res
 */
async function updateExample(req, res) {
    const { user } = req;
    const { exampleId } = req.params;
    const { name } = req.body;

    if (!name) {
        return res.status(400).json({ message: 'Bad Request: Fields (name) are missing from request body', statusCode: 400 });
    }

    try {
        // Update example by id
        const updatedExample = await Example
            .findByIdAndUpdate(exampleId,
                { $set: { name, updatedBy: user.id, updatedDate: new Date() } },
                { new: true, select: selectedFields });

        if (!updatedExample) {
            return res.status(404).json({ message: `Example: "${exampleId}" not existed`, statusCode: 404 });
        }

        res.json(updatedExample);
    } catch (error) {
        res.status(500).json({ message: `An unexpected error occurred while try to update example: "${exampleId}"`, statusCode: 500 });
        logger.error(`Error while try to update example: "${exampleId}" in updateExample api`, error, req);
    }
}

/**
 * Delete example
 *
 * @param {express.Request} req
 * @param {express.Response} res
 */
async function deleteExample(req, res) {
    const { exampleId } = req.params;

    try {
        const deletedExample = await Example
            .findByIdAndRemove(exampleId,
                { select: selectedFields });

        if (!deletedExample) {
            return res.status(404).json({ message: `Example: "${exampleId}" not existed`, statusCode: 404 });
        }

        res.json(deletedExample);
    } catch (error) {
        res.status(500).json({ message: `An unexpected error occurred while try to delete example: "${exampleId}"`, statusCode: 500 });
        logger.error(`Error while try to delete example: "${exampleId}" in deleteExample api`, error, req);
    }
}

/**
 * exampleId middleware
 *
 * @param {express.Request} req
 * @param {express.Response} res
 * @param {express.NextFunction} next
 */
async function exampleIdMiddleware(req, res, next) {
    const { exampleId } = req.params;

    if (!ObjectId.isValid(exampleId)) {
        return res.status(400).json({
            message: `ExampleId: "${exampleId}" is invalid`,
            statusCode: 400
        });
    }

    next();
}

module.exports = {
    getExamples,
    getExampleById,
    createExample,
    updateExample,
    deleteExample,
    exampleIdMiddleware
};
