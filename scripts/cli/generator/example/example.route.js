const express = require('express');
const { getExamples, getExampleById, createExample, updateExample, deleteExample, exampleIdMiddleware } = require('./example.controller');

/**
 * Example routes
 *
 * @param { express.Express } app express application
 */
module.exports = (app) => {
    app.route('/api/example')
        .get(getExamples)
        .post(createExample);

    app.route('/api/example/:exampleId')
        .all(exampleIdMiddleware)
        .get(getExampleById)
        .put(updateExample)
        .delete(deleteExample);
};
