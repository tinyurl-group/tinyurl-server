const { camelCase, kebabCase, startCase } = require('lodash');
const { plural, } = require('pluralize');
const { readFile, writeFile, mkdir, exists } = require('fs');
const { promisify } = require('util');
const { map } = require('bluebird');

const readFileAsync = promisify(readFile);
const writeFileAsync = promisify(writeFile);
const mkdirAsync = promisify(mkdir);
const existsAsync = promisify(exists);

async function generateCrudApi(modelName) {
    const modelNameKebabCase = kebabCase(modelName); // ModelName => model_name
    const modelNameCamelCase = camelCase(modelNameKebabCase); // model_Name => modelName
    const modelNameCamelCasePlural = plural(modelNameCamelCase); // modelName => modelNames
    const modelNameCapital = startCase(modelNameKebabCase).replace(/ /g, ''); // model_name => ModelName
    const modelNameCapitalPlural = plural(modelNameCapital); // ModelName => ModelNames

    try {
        const filesPromises = ['controller', 'route', 'model'].map(fileType => readFileAsync(`./scripts/cli/generator/example/example.${fileType}.js`, { encoding: 'utf-8' }));
        const [controller, route, model] = await map(filesPromises,
            (fileData) => {
                let newFileData = fileData;

                newFileData = newFileData.replace(/examples/g, modelNameCamelCasePlural);
                newFileData = newFileData.replace(/example/g, modelNameCamelCase);

                newFileData = newFileData.replace(/Examples/g, modelNameCapitalPlural);
                newFileData = newFileData.replace(/Example/g, modelNameCapital);

                return newFileData;
            });

        const folderExists = await existsAsync(`./api/${modelNameCamelCase}`);

        if (folderExists) {
            return console.log(`Folder "/api/${modelNameCamelCase}" already exists`);
        }

        await mkdirAsync(`./api/${modelNameCamelCase}`);

        const files = { controller, route, model };
        await Promise.all(Object.keys(files)
            .map(fileType => writeFileAsync(`./api/${modelNameCamelCase}/${modelNameCamelCase}.${fileType}.js`, files[fileType], { encoding: 'utf-8' })));

        console.log(`${modelNameCamelCase} api successfully created`);
    } catch (error) {
        console.error(error);
    }
}


module.exports = {
    generateCrudApi
};
