# using node:10 image
FROM node:10

# Set environment variables
ARG PORT=8080
ARG NODE_ENV=production

# Set the work directory
WORKDIR /var/www/tinyurl

# Install dependencies
COPY package.* /var/www/tinyurl
COPY package-lock.json /var/www/tinyurl
RUN npm install --production

# Add application files
COPY . /var/www/tinyurl

# Expose the app port
EXPOSE $PORT

CMD ["node", "server.js"]
