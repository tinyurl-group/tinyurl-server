
module.exports = {
    db: {
        mongo: {
            uri: process.env.MONGO_URI || 'mongodb://localhost:27017/tinyurl',
            options: {
                useNewUrlParser: true,
                config: {
                    autoIndex: false
                }
            },
            // Enable mongoose debug mode
            debug: process.env.MONGO_DEBUG ? (process.env.MONGO_DEBUG === 'true') : false
        }
    },
    bots: {
        appBotToken: process.env.BOT_TOKEN
    }
};
