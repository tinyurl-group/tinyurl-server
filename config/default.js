

const config = {
    app: {
        title: 'tinyurl',
        description: 'tinyurl description'
    },
    env: process.env.NODE_ENV || 'development',
    isProd: process.env.NODE_ENV === 'production',
    port: parseInt(process.env.PORT || '8080'),
    host: process.env.HOST || '0.0.0.0',
    url: process.env.URL,
    logger: {
        debug: process.env.LOG_DEBUG ? (process.env.MONGO_DEBUG === 'true') : true,
        mail: {
            username: process.env.LOGGER_MAIL_USERNAME,
            password: process.env.LOGGER_MAIL_PASSWORD,
            from: process.env.LOGGER_MAIL_FROM,
            to: process.env.LOGGER_MAIL_TO ? process.env.LOGGER_MAIL_TO.split(',') : [],
        }
    },
    mailer: {
        from: process.env.MAIL_FROM || 'tinyurl <tinyurl@gmail.com>',
        service: 'gmail',
        auth: {
            user: process.env.MAIL_USERNAME,
            pass: process.env.MAIL_PASSWORD
        }
    }
};

// Set app url
config.url = config.url || `http://${config.host === '0.0.0.0' ? 'localhost' : config.host}:${config.port}`;

module.exports = config;
