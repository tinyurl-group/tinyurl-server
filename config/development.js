
module.exports = {
    db: {
        mongo: {
            uri: process.env.MONGO_URI || 'mongodb://localhost:27017/tinyurl-dev',
            options: {
                useNewUrlParser: true,
                config: {
                    autoIndex: true
                }
            },
            // Enable mongoose debug mode
            debug: process.env.MONGO_DEBUG ? (process.env.MONGO_DEBUG === 'true') : true
        }
    },
    bots: {
        appBotToken: process.env.BOT_TOKEN
    }
};
